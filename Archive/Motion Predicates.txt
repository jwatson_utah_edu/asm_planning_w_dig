If you make an assumption that hinders or invalidates your method, then you either need to change your method or not rely on that assumption.

~~~~~~ Base System ~~~~~~
Need this to run, could I solve any problems here?

[ ] Base ASP + Geo Reasoning
    * Use hemispheres as a compact representation of local freedom after WAN & HARADA
    * Two connection graphs are required: [ ] Explain why it would be a mistake to conflate these two
        A. Connection graph, which represents the designed connections between parts
        B. Liaison graph, which represents the part adjacencies that represent local freedom
    1. Determine the nature of contact , 
        | | What happens if we assume that all meshes are composed of planes? : Could potentially support both grasp search and local freedom
            ! ! Game out the consequences of assuming that cylinders and spheres are approximated with large planes - For example, wouldn't
                creating a bunch of broad planes cause the compressed shape to occupy more space than it really does, and cause false 
                negatives in the validation phase?
        * Planar contact: Planes with opposing normals that are within some margin of each other
    2. Determine the local freedom constraints with hemispheres , one for each planar contact
    3. Intersection of freedom hemispheres is the local freedom and directly relates to what parts are in the state


~~~~~~ Superproblem ~~~~~~
The capabilities of the assembler ( robot / human ) must be considered if the 


[ ] Problem: An assembly action from the planner might be infeasible due to the inability to move the manipulator to a pose that will
             put the moving part in its target pose.
             
             
             
[ ] Problem: Force closure must be able to support the part as it is assembled



[ ] Problem: Force closure and the capability of the robot must be able to supply the forces



[ ] Problem: Feasibility predicates typically consist of either axes-aligned motions or full motion plans. There is must be a middle ground
             that efficiently represents the feasibility of a motion plan
    * SIMPLE:  Translate the part from the beginning pose along straight rays that are sampled from the local freedom representation
    * COMPLEX: Compute a feasible trajectory on a discretized state lattice that is assumed to be representative of all the poses that
               the moving part can assume              
