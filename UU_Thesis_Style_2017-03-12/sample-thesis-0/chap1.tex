%%% -*-LaTeX-*-

\chapter{Introduction}
Assembly Planning concerns the ordering and planning of actions that bring separate parts together to form a complete, complex product assembly \cite{simplifiedGenAllSeq_GEARTRAIN}.  The study of assembly planning is important to both the human performance of assembly on the factory floor \cite{PostProcessingRafiCampbell}, and to the development of fully autonomous industrial robotics systems \cite{plansForRobotExec}.  Assembly Sequence Planning (ASP) is a subproblem of assembly planning that focuses on establishing the order in which to assemble parts, given some suitable planner for the individual motions \cite{simplifiedGenAllSeq_GEARTRAIN}.  

The identification of subassemblies within a product assembly is central to assembly planning.  Treating subassemblies as self-contained subproblems allows for parallel execution of logical, noninterfering plans by multiple workers.  This, in turn, decreases total execution time on the factory floor.  Product assembly accounts for over half of total production time and more than one third of the total production cost in industrial manufacturing \cite{mnfgTime}.  Therefore, reducing assembly time remains one of the chief aims of assembly planning.

A common approach to ASP is disassembly sequence planning (DSP), which switches the start and goal states, working backwards from a completed product to the individual parts \cite{ANDOR_original,taxonomyOverview,validationBlock_kheder2017}.  This reduces the complexity of planning even further by beginning from a state that imposes the most constraints on choices of action at the beginning of the problem \cite{taxonomyOverview}.  Our method and the comparative methods all employ the DSP strategy.

We hypothesize that partitioning a product into subassemblies in a way that respects both assembly constraints and part access will result in assembly plans that are time-efficient in the number of actions required to execute the generated plans.  Existing assembly planning methods deal with part access as a separate validation step \cite{IKEA,kmeansRRT45,volumeAreaFitness2}.  Our method addresses part access as a direct, quantitative measure.  We are able to form subassemblies that exhibit the following properties: correctness (no assembly constraints violated) and reduction in part obstruction at every step.  Identifying subassemblies that satisfy these properties promotes more efficient assembly.

Our main contribution is a graph structure, which estimates the interference that each part presents to the disassembly of all other parts, called the disassembly interference graph (DIG).  We exploit this graph to effectively partition complex assemblies into logical subassemblies that respect correct assembly precedence and yield efficient disassembly sequence plans according to some metrics.  We accomplish this result without having to exhaustively search the massive space of possible sequences.

Our method aims to estimate the degree to which one part obstructs the removal of another part.  We do this by defining a removal space for each part, and measuring how that space is impinged upon by surrounding geometry.  This numeric representation of part obstruction is the basis of the DIG.

To show our method's efficacy, we compare it to two other subassembly partition methods \cite{kmeansRRT45,volumeAreaFitness2}.  We evaluate performance in terms of the number of actions required to execute the resulting sequences.  The other methods focus on geometric properties of parts and their contacts \cite{volumeAreaFitness,volumeAreaFitness2}, connection graph connectivity \cite{CollsnCheckSubsOnly}, and geometric proximity \cite{kmeansRRT45}.  The comparative methods use motion planning to validate subassembly choices, but do not consider part interaction as part of the subassembly choice.

The remainder of this paper is organized as follows: Chapter \ref{SECT_related} is a short summary of related work in assembly planning, especially as it pertains to subassembly partitioning.  Chapter \ref{SECT_methods} provides details of our methods, including the construction of the disassembly interference graph.  Chapter \ref{SECT_experiment} describes the experiments conducted in order to compare assembly planning methods.  Chapter \ref{SECT_results} discusses the results of the experiments.  Chapter \ref{SECT_conclusion} summarizes our conclusions from the results.  Chapter \ref{SECT_THEFUTURE} describes possible future work.

%%% Index phrases should be attached to an important word of a phrase,
%%% and are usually best kept on a separate line by terminating the
%%% previous line with a percent comment without intervening space, as
%%% in this example:
%%%
%%%     \newcommand {\X} [1] {#1\index{#1}}
%%%
%%%     African ungulates,%
%%%     \index{African ungulate}
%%%     like the \X{gnu}, \X{impala}, \X{kudu}, and \X{springbok}
%%%     live mostly in hot climate and consume vegetation.
%%%
%%% However, for this document, we only want lots of index entries to
%%% populate a sample topic index.

%%% ====================================================================
%%% Cross-references for index entries should be specified only once:
