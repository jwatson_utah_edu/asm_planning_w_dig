\contentsline {chapter}{\uppercase {Abstract}}{iii}
\contentsline {chapter}{LIST OF FIGURES}{vi}
\contentsline {chapter}{LIST OF TABLES}{vii}
\contentsline {chapter}{\uppercase {Notation and Symbols}}{viii}
\unhbox \voidb@x \\[-1.2ex] \kern -1.5em\contentsline {}{\textbf {CHAPTERS}}{}
\contentsline {chapter}{\numberline {1.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Introduction}\global \uuthesis@needtocspacetrue }{1}
\contentsline {chapter}{\numberline {2.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Related Work}\global \uuthesis@needtocspacetrue }{3}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {section}{\numberline {2.1}Subassembly Identification}{3}
\contentsline {section}{\numberline {2.2}Approaches to ASP}{4}
\contentsline {subsection}{\numberline {2.2.1}Graph Methods}{4}
\contentsline {subsection}{\numberline {2.2.2}Soft Search Methods}{4}
\contentsline {subsection}{\numberline {2.2.3}Hybrid Methods}{5}
\contentsline {chapter}{\numberline {3.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Methods}\global \uuthesis@needtocspacetrue }{6}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {section}{\numberline {3.1}Intuition \& Motivation}{6}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {subsection}{\numberline {3.1.1}Development of the Method}{7}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {subsection}{\numberline {3.1.2}Removal Space}{7}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {subsection}{\numberline {3.1.3}Blocking Fraction}{8}
\contentsline {section}{\numberline {3.2}Calculation of Blocking Fraction}{10}
\contentsline {section}{\numberline {3.3}Disassembly Influence Graph}{12}
\contentsline {section}{\numberline {3.4}Subassembly Identification by Blocking Reduction}{12}
\contentsline {section}{\numberline {3.5}Assumptions and Constraints}{15}
\contentsline {section}{\numberline {3.6}Geometric Pre-Processing}{16}
\contentsline {section}{\numberline {3.7}Path Planning for Action Validation}{19}
\contentsline {chapter}{\numberline {4.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Experiments}\global \uuthesis@needtocspacetrue }{20}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {section}{\numberline {4.1}Tested Assemblies}{20}
\contentsline {section}{\numberline {4.2}Compared Methods}{20}
\contentsline {subsection}{\numberline {4.2.1}Part Interaction Clusters, Morato 2013}{21}
\contentsline {subsection}{\numberline {4.2.2}Subassembly Generation from CAD, Belhadj 2017}{22}
\contentsline {section}{\numberline {4.3}Assembly Workcell Simulation}{25}
\contentsline {section}{\numberline {4.4}Measures for Evaluation}{26}
\contentsline {chapter}{\numberline {5.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Results}\global \uuthesis@needtocspacetrue }{28}
\ifuuthesis@needtocspace \vspace {\uuthesis@chaptersectionspace } \fi \global \uuthesis@needtocspacefalse 
\contentsline {section}{\numberline {5.1}Disassembly Plans}{28}
\contentsline {section}{\numberline {5.2}Assembly Workcell Simulation}{28}
\contentsline {section}{\numberline {5.3}Quantitative Results}{31}
\contentsline {section}{\numberline {5.4}Algorithm Behavior}{31}
\contentsline {chapter}{\numberline {6.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Conclusion}\global \uuthesis@needtocspacetrue }{35}
\contentsline {chapter}{\numberline {7.}\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {Future Work}\global \uuthesis@needtocspacetrue }{36}
\contentsline {chapter}{\hyphenpenalty =10000\exhyphenpenalty =10000\relax \linepenalty =0 \uppercase {APPENDIX: Appendix A}\global \uuthesis@needtocspacetrue }{37}
\contentsline {chapter}{REFERENCES}{42}
