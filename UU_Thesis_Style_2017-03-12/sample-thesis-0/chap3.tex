%%% -*-LaTeX-*-

\chapter{Methods} \label{SECT_methods}
We now present our disassembly sequence planner, which takes as input a fully assembled product as a start state, and returns a tree structure of disassembly actions required to bring it to its fully disassembled goal state.  The correctly-generated tree can be executed in reverse to produce an assembled product via geometrically feasible actions.

\section{Intuition and Motivation}
The basis for our method is the intuition that the ability to execute an assembly depends on access to individual parts.  Part access can be modeled using the assembly geometry.  Following this intuition, we have developed a method that identifies subassemblies in a way that prioritizes part access.  Part access can also be used to establish precedence among identified subassemblies.

In order to model part access, we define a 3-dimensional volume for each part called a removal space.  Our method is based on the idea that local movement constraints between parts imply a removal space representing the region of logical removal paths.  Once this region is defined, it can be evaluated for obstructions that might impair disassembly actions, without generating and evaluating individual paths.  Using the model, part access, or conversely part blockage, can be measured as a scalar quantity.  

Our method decomposes a complete assembly into subassemblies whenever possible, and only falls back on single-part operations when no such groupings can be made.  There are two main benefits to subdividing the disassembly problem in this way.  The first is that it allows the problem to be solved recursively, with each subassembly being treated as a small disassembly problem.  The second is that independent subassemblies may be executed in parallel by different workers. 

Generating a disassembly plan that prioritizes part access proceeds along the following steps: determining removal spaces for parts of the assembly, choosing subassemblies with removal spaces free from interference, and executing removal actions with the freest part access first.

\subsection{Development of the Method}
Having established the concept of removal space, we define it geometrically as described in Section \ref{SECT_removalSpace}.  We can use this geometric definition to develop a scalar heuristic for the degree to which the removal space is obstructed in a way that might frustrate disassembly.  We call this heuristic the blocking fraction, and it is described in Section \ref{SECT_disasmInterfere}.  It is not enough to know whether a part is blocked, we must know which parts block which other parts.  The pairwise definition of blocking fraction has advantage of allowing us to immediately calculate the degree to which any collection of parts obstructs any other collection of parts.  In this way, we can compare the ease of removing one subassembly versus another without having to reconstruct the removal space for each scenario.  The blocking fraction for every pair of parts, in both directions, can either expressed as a graph that we call the Disassembly Influence Graph (DIG).  We store this graph as a matrix so that calculation of part and subassembly obstruction can be performed as matrix operations.

Our method partitions a total assembly into subassemblies by identifying parts as candidate bases.  Parts accrue to the bases if the estimated blockage of the presumptive subassembly is not increased by the addition.  Calculation of the blocked state (total blockage) using the DIG is described in Section \ref{SECT_DIGtotalBlock}.  In this way, we aim to construct subassemblies that can be removed easily as units.  Once subassemblies are defined, we choose which to remove at each level of decomposition if they are below some total blockage threshold.  This process recurs on every successfully removed subassembly until the product is fully decomposed.

\subsection{Removal Space} \label{SECT_removalSpace}
Defining the removal space is the first step in quantifying part access.  The removal space models a continuous space of possible translations of a part away from its specified relative pose within the complete assembly.  (For simplicity, subassembly motion is constrained to straight lines.)  If only translations without direction changes are allowed, then the shape of this region is an infinite cone (spherical pyramid) extending from the part origin.  

Consider a cube resting on a plate with a notch cut out of it (Figure \ref{simpleCube}).  It may be translated away from the plate vertically,  horizontally along either of the notch walls, or any direction that has a positive dot product with all of the notch face normals.  These translation constraints may be visualized as the spherical pyramid in Figure \ref{removalSpace}.   This is what is represented by the extended translational freedom cone in \cite{freedomCone}.  The removal space for a part is a region that begins at the part's assembled location and radiates out along possible translation paths as it exits the assembly.  If there are other parts found in this cone, then some of the translational removal paths are obstructed.  The more obstructed this region is, the more difficult it is to remove the part.  This is true even if the obstructing part does not impose a translation constraint on the part in its specified pose.  That is, a part that is free to move locally may not be free to be removed from the assembly.
\begin{figure}[b]
	\centering
	%\begin{minipage}{0.45\textwidth}
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{paperGraphics/simpleCube.png} 
		\caption{ } \label{simpleCube}
		%\end{minipage}\hfill
	\end{subfigure}
	\hspace*{\fill} % separation between the subfigures
	%\begin{minipage}{0.45\textwidth}
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{paperGraphics/removalSpace.png} 
		\caption{ }
		\label{removalSpace}
		%\end{minipage}\hfill
	\end{subfigure}\hfill
	\caption{Development of removal space: (a) A cube rests in a notch cut from a cuboid plate. The notch allows translation up, forward-left along its nearest wall, backward left along its farthest wall, or any combination of these.  (b) Removal space of the cube shown in Figure \ref{simpleCube}. Any directed line segment from the cube origin to the curved surface of the spherical pyramid is a translation allowed by the constraints on local freedom.}
\end{figure}

\subsection{Blocking Fraction} \label{SECT_disasmInterfere}
We provide a heuristic for estimating the degree to which one Part $j$ obstructs another Part $i$ from being disassembled, called the blocking fraction ($w_{ij}$).  We define the blocking fraction $w_{ij}$ as an expression of the degree to which the removal space of a Part $i$ is obstructed by another Part $j$.  Blocking fraction may be thought of as the complement of part access.

The modeled removal space of a part or subassembly is a continuous 3-dimensional volume (Section \ref{SECT_removalSpace}), and contains an infinite number of possible removal paths for the subassembly.  In order to assess the extent this space is blocked, a simplification is made: the removal space is discretized to a collection of concentric surfaces (shells) radiating from the moving subassembly center.  Construction of the concentric shells begins at the interior of the subassembly because the presence of neighboring (potentially blocking) parts within concavities of the subassembly must be considered. 
\begin{figure}[b]
	\centering
	\includegraphics[scale=0.30]{paperGraphics/concentricShells.png}
	\caption{Intersection surfaces, spherical pyramid, constructed for the case in Figure \ref{simpleCube}.  Our method assumes that in order to be removed, the cube will pass through the intersection surfaces that occupy \sfrac{1}{8} of a sphere.}
	\label{sphereShell}
\end{figure}
A part translating from its original location to the exterior of the assembly must pass through each of the shells, as can be seen from Figure \ref{sphereShell}.  At each shell distance $d$ from the starting position, regions of the shell that are in intersection with surrounding assembly geometry represent positions unavailable for the part to occupy on its way out.  The more this surface intersects the surrounding assembly, the fewer paths are available for the part to escape the assembly.  The maximum fraction of the surface area of any shell that is in collision with another part's geometry is the blocking fraction.  

For parts with 3 degrees of translational freedom, shells are represented as polyhedral approximations of spherical sections constructed using Tegmark's method \cite{geodesic}.  For parts with fewer than 3 degrees of translational freedom, a shell more appropriate to the part's movement is chosen (Table \ref{TBL_freedomClass}).

Note that the constructed shell does not represent the true set of positions available for the moving part to occupy, as it does not consider intersection with configuration-space obstacles as in \cite{CSpaceObstacles}.  Calculating the true set of available or unavailable part positions is not required for our method because the main purpose of the proposed heuristic is to compare the the access of parts in the assembly, not to generate motion plans.

The spherical pyramid representation of free removal space is not new in ASP.  Romney analyzes the the geometry of assemblies to generate an extended translational freedom cone in \cite{freedomCone}.  Thomas et al. \cite{projectionCone2p5D} use a cone in configuration space to determine the feasibility of part removal operations.   Our method is differentiated from the usual in that we compute the obstruction of a cone of probable removal directions, and treat this blockage as a cost to be reduced.

\section{Calculation of Blocking Fraction}
Calculation of the blocking fraction for part $P_i$ vs $P_j , i \neq j$ proceeds in three steps: determination of freedom type from local part constraints, generation of concentric surfaces, and calculating intersection of parts and surfaces.  The constraints imposed by adjacent parts $P_j$ (reference assembly) determine the degrees of freedom of $P_i$ (moving assembly).  Our system can identify several types of freedom scenario ranging from 0 to 3 degrees of translational freedom.
\begin{figure}[b]
	\centering
	\captionof{table}{Freedom Classes, Ordered by Decreasing Constraints} \label{TBL_freedomClass} 
	\begin{tabular}{|l|r|l|}
		\hline
		\textbf{Freedom Class}    & \textbf{DOF} & \textbf{Intersection Surface} \\ \hline
		Locked                    & 0            & Sphere                     \\ \hline
		Prismatic, Unidirectional & 1            & Planar Projection          \\ \hline
		Prismatic, Bidirectional  & 1            & Planar Projection          \\ \hline
		Planar, Constrained       & 2            & Revolved Line ( $<2\pi$ )  \\ \hline
		Planar, Unconstrained     & 2            & Revolved Line ( $2\pi$ )   \\ \hline
		Spherical Pyramid         & 3            & Hemisphere Intersection    \\ \hline
		Dihedral                  & 3            & Spherical Slice            \\ \hline
		Half-Space                & 3            & Hemisphere                 \\ \hline
		Free                      & 3            & Sphere                     \\ \hline
	\end{tabular} 
\end{figure}
If the part $P_i$ has a nonempty local freedom allowance (see Section \ref{geoNDBG}), then a surface (triangle mesh) is projected from the part at regular intervals.  The freedom class determines the shape of the projected surface, as specified in Table \ref{TBL_freedomClass}.  The intersection surface for the prismatic freedom types is a projection of the part (or assembly) onto a plane perpendicular to $P_i$'s degree of freedom direction (Figure \ref{uniShell}).
\begin{algorithm}[t]
	\caption{Blocking Fraction, with Moved Part $P_{i}$ and Reference Part $P_{j}$} \label{calcBlockingFrac}
	\begin{algorithmic}[1]
		\Procedure{Blocking Fraction}{$P_{i}$, $P_{j}$, NDBG}
		\begin{flushleft}
			\textbf{INPUT:} Part $P_{i}$, Part $P_{j}$, NDBG \\
			\textbf{OUTPUT:} Blocking Fraction $w_{ij} \in \left[0,1\right]$
		\end{flushleft}
		\State $w_{ij} = 0$
		\State {\tt shells = construct\_shells(} $P_i$ , NDBG {\tt )}
		\For{ {\tt shell $s$ in shells} }
		\State $A_{block}$ \larw {\tt mesh\_intersection(} $s$ , $P_j$ {\tt )}
		\State $A_{s}$ \larw {\tt area\_of(} $s$ {\tt )}
		\State $w_{ij}$ \larw $\max( w_{ij} , \sfrac{A_{block}}{A_s} )$
		\EndFor
		\State \Return $w_{ij}$
		\EndProcedure
	\end{algorithmic}
\end{algorithm}
The planar freedom types are characterized by one or more pairs of opposing constraints.  An axis collinear with the opposing constraints forms the axis of the surface of a revolved line segment.  The height of the segment is the extent of $P_i$'s projection onto the segment.  For the constrained planar type, the constraints on $P_i$ determine the arc that the segment is revolved through (Figure \ref{planarShell}).  Intersection surfaces for the remaining freedom types can be constructed from sections of a spherical surface \cite{geodesic} (Figure \ref{sphereShell}).
\begin{figure}[b]
	\centering
	\includegraphics[scale=2.0]{paperGraphics/liftShadow.png}
	\caption{Intersection surfaces, prismatic unidirectional. The dark pink block sits in a recess that only allows translation in one direction.  The green block obstructs its path, and intersects approximately 34\% of the projected surface area, so 0.34 is the blocking fraction.}
	\label{uniShell}
\end{figure}
Construction of intersection surfaces begins in the interior of the part, and the intervals between surfaces must be spaced sufficiently close together to correctly show when one part passes through the interior of another.  At each interval surface ($s$ in Algorithm \ref{calcBlockingFrac}), the fraction of the surface that intersects the blocking part geometry is calculated ($\sfrac{A_{block}}{A_s}$).  Facets of the intersection surface are considered in collision if there is a triangle-triangle intersection between the surface and the part, or if a line segment from the original part location to the surface facet center intersects the part mesh in between the endpoints of the segment.  The maximum fraction of any shell surface in collision with $P_j$ is the blocking fraction $w_{ij}$ that $P_j$ imposes on $P_i$.  $w_{ij}$ must be in the interval $[0,1]$.

If the part $P_i$ is locked, then its freedom state does not suggest any free removal directions.  Concentric spheres are used as the intersection surfaces, and the blocking fraction versus each part calculated as described above.
\begin{figure}[t]
	\centering
	\includegraphics[scale=0.30]{paperGraphics/tasteTheRainbow.png}
	\caption{Intersection surfaces, planar constrained.  The light blue block is constrained from behind, below, and on either side.  Its possible translation directions are found on the 90$^\circ$ arc shown, so its intersection surfaces are revolved line segments constrained to this arc and of the same width as the part.}
	\label{planarShell}
\end{figure}
\begin{figure}[b]
	\centering
	\includegraphics[scale=0.50]{paperGraphics/A_accrues_B.png}
	\caption{Part $A$ cannot be removed alone, but a subassembly consisting of $A$ and $B$ is identified as having less total blockage than the sum of blockages of the individual parts.}
	\label{A_accrues_B}
\end{figure}

\section{Disassembly Influence Graph} \label{SECT_DIGtotalBlock}
We define the disassembly influence graph (DIG) here as a means of describing the degree to which each part blocks every other part from being removed from the product assembly.  The vertices $P_D$ represent all of the parts in the product assembly in their relative poses in the completed product, and directed edges $e(P_j,P_i) \in E_D$ represent the degree to which $P_i$ is blocked from removal by $P_j$.  The weight $w_{ij} \in W_D$ represents the blocking fraction $P_i$ that intersects $P_j$.  The DIG has $n^2$ weights $w_{ij}$ and can be represented by an $n \times n$ matrix (Algorithm \ref{asmNRG}).  In general, this matrix is not symmetric.  The interference measure can also be calculated for collections of subassemblies.  

The blocking fraction describes the blocked relationship between $P_i$ and $P_j$.  The total blockage $\tau_i$ for part $i$ describes the degree to which a single part or subassembly is blocked by all other parts, and it is defined by
\begin{align}
\tau_i = \sum_{j=1}^{N}\left[ w_{i,j} \right]
\end{align}
\noindent where $N$ is the total number of parts in the assembly considered. The quantity $\tau_i$ can be greater than one, which can happen either when the part $i$ has many partial blockages along its possible exit paths, or when it is completely covered by more than one part.  A part that is completely or partially obscured by many other parts is deprioritized for removal (see Section \ref{SIR}.).

\begin{algorithm}
	\caption{Disassembly Interference Graph for an Assembly} \label{asmNRG}
	\begin{algorithmic}[1]
		
		\Procedure{DIG}{Assembly State S}
		
		\begin{flushleft}
			\textbf{INPUT:} Assembly part geometry, relative poses, liaison graph \\
			\textbf{OUTPUT:} Disassembly Influence Graph
		\end{flushleft}
		
		\State All $w_{ij} = 0$ \Comment{Init}
		\For{each part $P_i$} \Comment{Blocked part}
		\If{$P_i$ has local freedom}
		\State Generate surfaces according to Table \ref{TBL_freedomClass}
		\Else \Comment{$P_i$ has \emph{no} local freedom}
		\State Generate concentric spheres to $(1.5)r_{part}$
		\EndIf
		\For{each part $P_j , j \neq i$} \Comment{Blocking part}
		\State $w_{ij} = \max\left( \left( A_{intersection} / A_{surface} \right) , w_{ij} \right) $
		\EndFor
		\EndFor
		\State \textbf{return} $E_{DIG}$
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\section{Subassembly Identification by Blocking Reduction} \label{SIR}
The primary aim of our method is to pull parts away from the the total assembly in groups that have the freest removal space.  A single part may be partially blocked by several neighbors, and cannot be removed.  However, if this part can be removed with its blocking neighbors as a unit, then it is a logical candidate for a subassembly.  Also, adjacent parts that can be removed as a unit become part of a subassembly.  Individual parts accrue neighbors whose addition does not increase the total blockage of a candidate subassembly (Figure \ref{A_accrues_B}).  After candidate subassemblies are generated, we calculate the total blockage for each as a unit.  Subassemblies that have a sufficiently free removal space are validated by simulation.
\begin{algorithm}
	\caption{Subassembly Identification by Blocking Reduction} \label{blockingReduceAlgo}
	\begin{algorithmic}[1]
		
		\Procedure{SubID by Blocking Reduction}{Assembly State S}
		
		\begin{flushleft}
			\textbf{INPUT:} Assembly part geometry, relative poses, liaison graph, NDBG \\
			\textbf{OUTPUT:} List of subassemblies
		\end{flushleft}
		
		\State Identify base
		\State Identify nucleus parts
		\State Calculate DIG using nucleus-base contact
		\For{Each nucleus part} 
			\State Accrue neighbors that do not increase blockage of subassembly
			\State Consider neighbors of added part
		\EndFor
		\State Accept subassemblies below blocking threshold
		
		\For{For each accepted subassembly}
			\State Validate subassembly removal
		\EndFor
		
		\State \Return Validated subassemblies
		
		\EndProcedure 
	\end{algorithmic}
\end{algorithm}
The first step in the subassembly identification process is to determine a base part (Algorithm \ref{blockingReduceAlgo}).  The part with the most volume serves as the base part for the overall assembly and is removed from consideration for subassemblies, as in \cite{kmeansRRT45}.  The remaining parts are graded for their fitness as the nuclei of subassemblies using the fitness score developed by Belhadj \cite{volumeAreaFitness}.  The largest gap in a score between any two part scores in a sorted list is chosen as the cutoff between nuclei parts and all others.  Then, the DIG is calculated for all nonbase parts (including nuclei), using only base-part constraints to determine the local freedom of each part.  The DIG is calculated using this freest state so that changes in blockage can be evaluated with the addition of each part to an assembly.  

Formation of the first subassembly begins by considering the nucleus part with the greatest base part fitness score. All of the immediate neighbors of the nucleus part are added to a priority queue, sorted by the degree to which each blocks the nucleus part (blocking fraction).  The freedom type and the total blockage for an assembly composed of the nucleus part and the prospective addition is calculated.  If the addition of the part frees a locked subassembly, the addition is automatically accepted.  Likewise, an addition that blocks a subassembly is automatically rejected.  If there was no change to or from a locked state, then a prospective part is accepted into a subassembly if the total blockage of the subassembly is less than or equal to the blockage without the new part.  If a part is accepted, then its neighbors are enqueued.  

The next most-blocking part is then popped from the queue, and the process repeats on parts that have neither been evaluated nor assigned to another subassembly.  Once the queue is emptied, the subassembly accumulation process is repeated on the nucleus part with the next-highest score.  The identification phase ends when all nuclei and their neighbors have been evaluated as described.  Any nonbase parts that remain unassigned at the end of the identification phase are grouped into a single remainder subassembly that includes the base. 

The total blockage of each of the identified subassemblies is evaluated with respect to the entire assembly.  Only subassemblies with a total blockage below a given threshold $\tau_i \leq f_{accept}$ are considered for removal.  We empirically found $f_{accept}=0.85$ to work well in our experiments.  Simulated removal is attempted for the subassemblies that meet the criterion, in directions sampled from the subassembly local freedom.  

The precedence layer \cite{kmeansRRT45} represents all of the disassembly operations that can be done with an assembly without respect to order.  That is, there is no precedence for removing a part or subassembly within the precedence layer.  Successive decomposition of the assembly into precedence layers results in a tree structure that represents the parallelism of the (dis)assembly process in a straightforward way.  This tree structure can be used to represent the output of all three subassembly identification methods.

Successor nodes of each subassembly within a layer can be done in parallel without interference.  It is possible to perform disassembly actions within the same precedence layer simultaneously \cite{HANDEY}, but this is a separate planning problem outside the scope of this work.

If the subassembly is able to be removed from the complete assembly, then it is added to the precedence layer.  Unsuccessful subassemblies are added back onto a remainder subassembly to be decomposed at the next recursion.

\section{Assumptions and Constraints} \label{SECT_assume}
\noindent We make the following assumptions:
\begin{enumerate}
	\item All parts are represented by closed, rigid polyhedral meshes with nominal dimensions. Tolerances are not modeled.
	
	\item Sequential, one-handed operations are assumed. Subassemblies are moved one at a time. The disassembly planner presented removes a subassembly (the moving parts) from its designed pose relative to the stationary portion of the assembly (the reference parts) to a pose completely separate from the original assembly. \label{asmp2}
	
	\item All assembly operations are monotonic. Two assembled parts do not move relative to one another once assembled.  Conversely, two parts do not move relative to each other until they are disassembled.  
	
	\item All assembly operations are reversible, and a valid removal operation implies that the reverse assembly operation is valid.
	
	\item Adjacent parts in the design are assumed to be connected and can support any other adjacent part. Stresses due to part contact are not considered.  Our experiments contain no designs with unconnected, free-floating parts. Fasteners are not modeled.
	
	\item  Contact dynamics are not modeled.  Connections between parts are rigid, with mating parts meeting at adjacent surfaces without deformation or penetration.
\end{enumerate}
We believe these assumptions are reasonable because most manufactured products contain a subset of parts that are immobile relative to one another.  
Fasteners are an essential part of manufactured products, but the purpose of this work, and the works it is compared to, is to establish a correct overall ordering of major parts in an assembly.  By design, fasteners are not unique and have many duplicates within a design.  For this reason, access for fastening operations can be treated as part of action validation \cite{FastenerPredicateAngryReviewer}.  Contact forces can cause some sequences to be nonreversible \cite{forceFlowDisasm,flexibleParts}, but this is treated as a separate problem.  The assumptions made for this work are the same as in the comparative works \cite{kmeansRRT45,volumeAreaFitness2}.

\section{Geometric Preprocessing} \label{geoNDBG}
The extraction of all part-to-part relationships and other relevant geometric data are prerequisites to our method and the compared methods.  The necessary information includes: relative translational freedom between all part-to-part pairs, convex hull, center of mass, and surface area.

The most important information is the relative translational freedom between parts, which can be represented by a Non-Directional Blocking Graph (NDBG) \cite{NDBGoriginal}.  We use this structure both to construct the geometry necessary to calculate the blocking fraction (Table \ref{TBL_freedomClass}), and to determine possible translation directions for action validation (Section \ref{validate}).  This structure is convenient for querying the removal direction constraints imposed on a part by all of its neighbors, or some subset of its neighbors.  Once constructed, the graph also allows the constraints on a collection of parts (a subassembly) imposed by any other subset of assembly parts to be queried easily.  Following the example of \cite{RegraspSphericalWanHarada}, local part freedom is represented as a unit sphere of possible translations radiating from the center, which represents the original location of the part.  Possible translation directions occupy a spherical pyramid, constrained by planes passing through the center of the sphere.  

Each planar surface contact between parts blocks a half-space of possible movement directions. Each of these blocking half-spaces are stored as normal vectors.  Of course, one part may present more than one constraint to any other part.  The local freedom of any part is the intersection of hemispheres of freedom obtained from each part in contact with that part.  The normals of blocking hemispheres imposed by neighboring parts are associated with part connections, such that the local freedom of parts can be quickly recalculated when part connections are made and broken.

Existing methods construct the NDBG by perturbing each part in some set of displacement directions and checking for collisions, or by analyzing surface contact between parts.  Ours is a hybrid of these approaches, beginning with identification of opposing flat surfaces, and then sampling on a sphere as is done in  \cite{OpposingFacetNormals,RegraspSphericalWanHarada,OpposingFacetNormals,InferringAsmDirectionsPCL,RestrictCollsnChecksSampleIsocahedron}.  This two-method approach is to account for the drawbacks of each.  A sampling of directions may not identify a prismatic degree of freedom between parts if the sampled directions do not lie along the axis of sliding contact.  Likewise, spherical direction sampling is unlikely to fall exactly parallel to a planar contact constraint (see Figure \ref{planarShell}).

Determining local freedom of parts begins with identifying flat surfaces (faces) on the part meshes using the clustering algorithm presented by Harada et al. \cite{clusterMeshGrasp}.  Then a search over the flat surfaces identifies those that are touching: that is, those that are adjacent and have opposing normal vectors.  This search is performed for every part-part pair in the assembly.  Local freedom constraints are assigned to parts corresponding to the normal of the adjacent face.  These constraints are stored in the Non-Directional Blocking Graph.  Then, we perturb each part along directions sampled on a sphere using Tegmark's method \cite{geodesic}.  When a collision is identified, then a constraint with a normal opposite of the direction of motion is added to the edge between the perturbed part and the part it collides with.  Finally, constraints that conflict with identified free directions are removed from each edge.

The approach described above will not correctly identify cylindrical (peg-in-hole) contact.  In order to remedy this, pegs are modeled with some radial clearance with the associated hole, and with the longitudinal axis along one of the coordinate axes of the peg.  We add perturbations along the local coordinate axes to the spherical sampling above.  These additional steps correctly identify prismatic translational freedom for cylindrical peg-in-hole features.

Our method enforces stability of subassemblies at each step.  A stable subassembly will be resting on a facet of its convex hull with the projection of the center of mass within that facet.  The convex hull is calculated using Antti Kuukka's implementation of Quickhull \cite{quickhull}.  The volume and center of mass is calculated using the signed tetrahedron volume method. 

\section{Path Planning for Action Validation} \label{validate}
Action validation proceeds in three stages.  In the first stage, the local freedom of all parts in the subassembly is checked.  If the freedom allowance of the assembly is empty, then the action validation fails.  This happens during subassembly identification, as detailed above.  The second stage is a search for a free translation path through task space in order to determine whether a certain action is feasible, considering collision with other parts and the table on which the assembly rests.

A correct disassembly sequence moves all parts from their relative positions in the complete assembly to any pose outside of the assembly without collision.  For a product with reversible assembly operations, these are equivalent \cite{forceFlowDisasm}.  Correctness is evaluated by generating a motion plan for each action, as described above.  If a noncolliding motion plan can be generated to satisfy the action, then the action is correct.