More information about the method adopted are desirable.

The method proposed and its comparison with other techniques seem do
not bring to really better result.
While the cited methods are in detail explained in the relative
documents, the text is not equally clear.
For example, it is not clear how the components to be disassembled are
selected and how to decide what is their disassembly sequence.
For this, I suggest to pay attention to explain in a more detailed
manner the selection technique adopted to the disassembly in order to
permit to the reader to evaluate the effectiveness of the proposed
method. 

Some orthographic mistakes have to be corrected and the organization of
the pictures position with respect to the text should be revisited.
 
Explain the measures adopted to define the disassembly plan efficiency
before to report the results could make the text more clear. 

I suggest you to include in the assembly pictures of the example
proposed some references (ballons?!) to the parts in order to better
clarify their disassembly sequences.

Reference to tables in the text, in particular "table I", would be
prefereable put them after the citation.   

Citation after " ...[6], connection graph connectivity [?], ..." is
missed.

Verify the text: Some "... the the ..." are repeated in the document
